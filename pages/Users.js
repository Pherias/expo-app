import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, StyleSheet, ActivityIndicator } from 'react-native';
import axios from 'axios';

import { BASE_URL } from '../config';
import authenticatedSession from '../authenticatedSession';

const Users = () => {
    const [users, setUsers] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchUsers = async () => {
            try {
                const response = await axios.get(`${BASE_URL}/users`);
                setUsers(response.data);
                setLoading(false);
            } catch (error) {
                console.error('Error fetching users:', error);
                setLoading(false);
            }
        };

        fetchUsers();
    }, []);

    const renderUserItem = ({ item }) => (
        <View style={styles.userCard}>
            <Text style={styles.userInfo}>ID: {item.id}</Text>
            <Text style={styles.userInfo}>Email: {item.email}</Text>
            <Text style={styles.userInfo}>First Name: {item.first_name}</Text>
            <Text style={styles.userInfo}>Last Name: {item.last_name}</Text>
        </View>
    );

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Users</Text>
            {loading ? (
                <ActivityIndicator size="large" color="#000000" />
            ) : (
                <FlatList
                    data={users}
                    keyExtractor={(item) => item.id.toString()}
                    renderItem={renderUserItem}
                />
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f0f0f0',
        position: 'relative',
        padding: 20
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
    },
    userCard: {
        backgroundColor: '#ffffff',
        borderRadius: 10,
        padding: 15,
        marginBottom: 15,
        elevation: 3,
    },
    userInfo: {
        fontSize: 16,
        marginBottom: 5,
    },
});

export default authenticatedSession(Users);
