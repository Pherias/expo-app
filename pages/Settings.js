import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import authenticatedSession from '../authenticatedSession';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useAuth } from '../AuthContext';

const SettingsScreen = ({ userData }) => {
  const { logout } = useAuth();
  const navigation = useNavigation();
  
  const handleLogout = () => {
    logout();
    navigation.replace('Login');
  };
  
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Settings</Text>

      <TouchableOpacity style={styles.backButton} onPress={() => handleLogout()}>
        <Text style={styles.buttonText}>Back</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    position: 'relative',
    padding: 20
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  backButton: {
    backgroundColor: '#e64132',
    padding: 10,
    borderRadius: 5,
    width: '80%',
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
  },
});

export default authenticatedSession(SettingsScreen);
