import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import authenticatedSession from '../authenticatedSession';

const DashboardScreen = ({ userData }) => {
  
  return (
    <View style={styles.container}>
      <Text style={styles.welcomeText}>Welcome {userData.first_name}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    position: 'relative',
    padding: 20
  },
  welcomeText: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
});

export default authenticatedSession(DashboardScreen);
