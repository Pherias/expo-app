import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import axios from 'axios';
import Modal from 'react-native-modal';
import { useAuth } from '../AuthContext'; // Replace with your actual path
import { BASE_URL } from '../config';

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState('x');
  const [password, setPassword] = useState('x');
  const [errorModalVisible, setErrorModalVisible] = useState(false);
  const { setToken } = useAuth(); // Use the useAuth hook

  const handleLogin = async () => {
    try {
      const response = await axios.post(`${BASE_URL}/token`, {
        email: email,
        password: password,
        grant_type: 'password',
      });

      if (response.status === 200) {
        // Successful login, handle the response
        const data = response.data;

        // Store the access token globally
        setToken(data.access_token);

        navigation.replace('Dashboard');
      } else {
        throw new Error(response.statusText)
      }
    } catch (error) {
      // Handle other errors, e.g., network issues
      setErrorModalVisible(true);
      console.error('Error during login:', error.message);
    }
  };

  const closeModal = () => {
    setErrorModalVisible(false);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Login</Text>
      <TextInput
        style={styles.input}
        placeholder="Email"
        value={email}
        onChangeText={(text) => setEmail(text)}
      />
      <TextInput
        style={styles.input}
        placeholder="Password"
        secureTextEntry
        value={password}
        onChangeText={(text) => setPassword(text)}
      />
      <TouchableOpacity style={styles.loginButton} onPress={handleLogin}>
        <Text style={styles.buttonText}>Login</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.backButton} onPress={() => navigation.replace('Home')}>
        <Text style={styles.buttonText}>Back</Text>
      </TouchableOpacity>

      {/* Error Modal */}
      <Modal isVisible={errorModalVisible} animationIn="slideInUp" animationOut="slideOutDown">
        <View style={styles.modalContent}>
          <Text style={styles.modalText}>Provided login information incorrect.</Text>
          <TouchableOpacity onPress={closeModal} style={styles.modalButton}>
            <Text style={styles.modalButtonText}>Close</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  input: {
    width: '80%',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 10,
    paddingLeft: 10,
  },
  loginButton: {
    backgroundColor: '#3498db',
    padding: 10,
    borderRadius: 5,
    width: '80%',
    alignItems: 'center',
    marginBottom: 10,
  },
  backButton: {
    backgroundColor: '#95a5a6',
    padding: 10,
    borderRadius: 5,
    width: '80%',
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
    alignItems: 'center',
  },
  modalText: {
    fontSize: 18,
    marginBottom: 10,
  },
  modalButton: {
    backgroundColor: '#3498db',
    padding: 10,
    borderRadius: 5,
    width: '80%',
    alignItems: 'center',
  },
  modalButtonText: {
    color: 'white',
    fontWeight: 'bold',
  },
});

export default LoginScreen;
