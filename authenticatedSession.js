import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity, StyleSheet, Text, ActivityIndicator, SafeAreaView, Platform, StatusBar } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { AntDesign } from '@expo/vector-icons';
import axios from 'axios';

import { BASE_URL } from './config';
import { useAuth } from './AuthContext';

const authenticatedSession = (WrappedComponent) => {
  return (props) => {
    const [userData, setUserData] = useState(null);
    const [loading, setLoading] = useState(true);
    const { accessToken, logout } = useAuth();
    const navigation = useNavigation();
  
    const navigateTo = (page) => {
      navigation.navigate(page)
    }

    useEffect(() => {
      const fetchUserData = async () => {
  
        if (!accessToken) return;
  
        try {
          const response = await axios.get(`${BASE_URL}/dashboard`, {
            headers: {
              Authorization: `Bearer ${accessToken}`,
            },
          });
  
          setUserData(response.data);
          setLoading(false);
        } catch (error) {
          console.error('Error fetching user data:', error);
        }
      };
  
      fetchUserData();
    }, [accessToken]);

    if (loading) {
      return (
        <View style={styles.loadingContainer}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    }

    return (
      <View style={{ flex: 1 }}>
        <View style={styles.navbar}>
          <TouchableOpacity style={styles.navbarButton} onPress={() => navigateTo('Dashboard')}>
            <Text style={styles.navbarButtonText}>Dashboard</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.navbarButton} onPress={() => navigateTo('Users')}>
            <Text style={styles.navbarButtonText}>Users</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.navbarButton} onPress={() => navigateTo('Settings')}>
            <Text style={styles.navbarButtonText}>Settings</Text>
          </TouchableOpacity>
        </View>

        <WrappedComponent {...props} userData={userData} />
      </View>
    );
  };
};

const styles = StyleSheet.create({
  navbar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#3498db',
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
  },
  navbarButton: {
    padding: 10,
  },
  navbarButtonText: {
    color: '#fff',
    textAlign: 'center',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default authenticatedSession;